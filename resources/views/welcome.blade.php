@extends('layouts.app')

@section('content')
    <div class="text-center">
        <img src="https://i.ibb.co/12wBGg4/icons8-laravel-96.png" alt="Laravel-Logo">
        <h3 class="text-center">FEATURED POSTS</h3>

        @foreach($posts as $post)
            <div class="card text-center">
                <div>
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                </div>
            </div>
        @endforeach
    </div>
@endsection
