@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>

			{{-- Likes and comments counts --}}
			@if((count($post->likes) > 0) || (count($post->comments) > 0))
				<div class="my-2 text-muted">
					@if(count($post->likes) > 0)
						@if(count($post->likes) == 1)
							<span class="mt-2">1 like</span>
						@else
							<span class="mt-2">{{count($post->likes)}} likes</span>
						@endif
					@endif
					@if(count($post->comments) > 0)
						@if(count($post->comments) == 1)
							<span class="mt-2">1 comment</span>
						@else
							<span class="mt-2">{{count($post->comments)}} comments</span>
						@endif
					@endif
				</div>
			@endif

			@if(Auth::id() != null) {{-- check if user is logged in --}}
				{{-- Like/Unlike --}}
				@if(Auth::id() != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						@if($post->likes->contains('user_id', Auth::id() ))
							<button type="submit" class="btn btn-danger">Unlike</button>
						@else
							<button type="submit" class="btn btn-success">Like</button>
						@endif
					</form>
				@endif
				
				{{-- Add Comment --}}
	 			<!-- Modal button -->
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
					Post Comment
				</button>
				<!-- Modal -->
				<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="commentModalLabel">Post a Comment</h5>
								<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
							</div>
							<div class="modal-body">
								<form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">
									@csrf
									<div class="form-group">
										<textarea class="form-control mt-2" id="content" name="content" rows="3"></textarea>
									</div>
									<div class="mt-2">
										<button type="submit" class="btn btn-primary">Add Comment</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			@endif

			{{-- Show comments --}}
			@if(count($post->comments) > 0)
				@foreach($comments as $comment)
					<div class="card mt-2">
						<div class="card-body">
							<span class="card-text mb-3">
								<strong>{{$comment->user->name}}</strong>
								<span class="text-muted">{{$comment->created_at}}</span>
								<p class="card-text">{{$comment->content}}</p>
								@if(Auth::id() == $comment->user_id)
									<form method="POST" action="/posts/{{$post->id}}/{{$comment->id}}/delete">
										@method('DELETE')
										@csrf
										<button type="submit" class="text-danger btn btn-link btn-sm">Delete Comment</button>
									</form>
								@endif
							</span>
						</div>
					</div>
				@endforeach
			@endif

			<div class=mt-3>
				<a href="/posts" class="card-link">View all posts</a>
			</div>
		</div>
	</div>
@endsection