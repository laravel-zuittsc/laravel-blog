<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{

    public function welcome()
    {
        //$posts = Post::all();
        //$featured = $posts->random(3);
        $posts = Post::inRandomOrder()
            ->where('isActive', true)
            ->limit(3)
            ->get();

        return view('welcome')->with('posts', $posts);
    }


    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        //if there is an authenticated user
        if(Auth::user()){
            //create a new Post object from the Post model
            $post = new Post;

            //define the properties of the $post object using received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            //get the id of the authenticated user and set it as the value of the user_id column
            $post->user_id = (Auth::user()->id);
            //save the post object to the database
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    public function index()
    {
        //get all posts from the database
        //$posts = Post::all();
        $posts = Post::all()->where('isActive', true);

        return view('posts.index')->with('posts', $posts);
    }

    public function myPosts()
    {
        if(Auth::user()){
            $posts = Auth::user()->posts; //retrieve the user's own posts

            return view('posts.index')->with('posts', $posts);

        } else {
            return redirect('/login');
        }
    }

    public function show($id) //will be receiving $id.. should be the same name used in routes
    {
        $post = Post::find($id);
        $comments = PostComment::all()->where('post_id', $post->id);
        return view('posts.show')->with('post', $post)->with('comments', $comments);
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    public function update(Request $request, $id) //pass both the form data in the request, as well as the id of the post to be updated
    {
        $post = Post::find($id);
        //if authenticated user's id is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }

        return redirect('/posts');
    }

    /*
    public function delete($id)
    {
        $post = Post::find($id);

        //if authenticated user's id is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->delete();
        }

        return redirect('/posts');
    }
    */

    public function archive($id)
    {
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            if($post->isActive == true){
                $post->isActive = false;
            } else {
                $post->isActive = true;
            }
                $post->save();
        }

        return redirect('/posts/myPosts');
    }

    public function like($id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        //check if the authenticated user is not the post author
        if($post->user_id != $user_id){
            //check if the post is already been liked by this user
            if($post->likes->contains('user_id', $user_id)){
                //delete the like made by the user to unlike the post
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else { //if the user has not liked the post yet
                //create a new postLike object
                $postLike = new PostLike;

                //define the properties of the postLike object
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                $postLike->save();
            }

            return redirect("/posts/$id"); //redirect the user to the specific post page
        }
    }

    public function comment(Request $request, $id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

       //create a new postComment object
        $postComment = new PostComment;

        //define the properties of the postComment object
        $postComment->post_id = $post->id;
        $postComment->user_id = $user_id;
        $postComment->content = $request->input('content');

        $postComment->save();

        return redirect("/posts/$id"); //redirect the user to the specific post page
    }

    public function deleteComment($id, $commentId)
    {
        $comment = PostComment::find($commentId);

        if(Auth::user()->id == $comment->user_id){
            $comment->delete();
        }

        return redirect("/posts/$id");
    }

}
