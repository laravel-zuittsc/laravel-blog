<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostLike extends Model
{
    use HasFactory;

    //Each PostLike belongs to a User
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    //ALSO... Each PostLike belongs to a Post
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
