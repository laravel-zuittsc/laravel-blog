<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    use HasFactory;

    //Each PostComment belongs to a User
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    //ALSO... Each PostComment belongs to a Post
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
