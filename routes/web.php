<?php

use Illuminate\Support\Facades\Route;
//Note-B: like import
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Models\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {

    $posts = Post::all();
    $featured = $posts->random(3);

    return view('welcome')->with('featured', $featured);
});
*/

//Rewrite the route for the welcome view
Route::get('/', [PostController::class, 'welcome']);

//route to return a view where the user can create a post
Route::get('/posts/create', [PostController::class, 'create']);

//route wherein theform data can be sent via POST method
Route::post('/posts', [PostController::class, 'store']);

//route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

//route that will return a view containing only the authenticated user's posts
Route::get('/posts/myPosts', [PostController::class, 'myPosts']);

//route that will show a specific post's view based on the URL parameter's id
Route::get('/posts/{id}', [PostController::class, 'show']);

//route that will allow the user to edit a specific post based on the URL parameter's id
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

//route that will overwrite an existing post with the form data submitted by the user
Route::put('/posts/{id}', [PostController::class, 'update']);

//route that will delete an existing post by the user
//Route::delete('/posts/{id}', [PostController::class, 'delete']);

//route that will archive/unarchive an existing post by the user
Route::put('/posts/{id}', [PostController::class, 'archive']);

//route that will enable users to like/unlike posts
Route::put('/posts/{id}/like', [PostController::class, 'like']);

//route that will enable users to comment in posts
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);

//route that will enable users to delete comments in posts
Route::delete('/posts/{id}/{commentId}/delete', [PostController::class, 'deleteComment']);

Auth::routes();

//Note-B: Request GET '/home'... specified controller will be triggered
/*Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');*/
Route::get('/home', [HomeController::class, 'index']);
